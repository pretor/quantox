<?php

namespace Quantox\Model;

use Quantox\Core\BaseModel;
use Quantox\Core\DBAL\DBManager;

/**
 * Class Student
 * @package Quantox\Model
 */
Class Student extends BaseModel
{

    /**
     * @var integer
     */
    private $student_id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var integer
     */
    private $grade_1;
    /**
     * @var integer
     */
    private $grade_2;
    /**
     * @var integer
     */
    private $grade_3;
    /**
     * @var integer
     */
    private $grade_4;

    /**
     * @var integer
     */
    private $schoolBoard;

    /**
     * @var array
     */
    private $settings = array(
        'fields' => ['name', 'grade_1', 'grade_2', 'grade_3', 'grade_4', 'school_board_id'],
        'table_name' => 'students',
        'sql' => '(`student_id` INT AUTO_INCREMENT NOT NULL, `name` varchar(200) NOT NULL,  `grade_1` INT NULL, `grade_2` INT NULL, `grade_3` INT NULL, `grade_4` INT NULL,  `school_board_id` INT NOT NULL,  PRIMARY KEY (`student_id`))',
        'key' => 'ALTER TABLE `students` ADD  CONSTRAINT `user_schoolboard`  FOREIGN KEY `user_schoolboard_fk` (`school_board_id`)   REFERENCES `school_board` (`school_board_id`)   ON DELETE CASCADE   ON UPDATE CASCADE;',
    );

    /**
     * @return int
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param int $student_id
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSchoolBoard()
    {
        return $this->schoolBoard;
    }

    /**
     * @param int $schoolBoard
     */
    public function setSchoolBoardId($schoolBoard)
    {
        $this->schoolBoard = $schoolBoard;
    }


    /**
     * @return int
     */
    public function getGrade1()
    {
        return $this->grade_1;
    }

    /**
     * @param int $grade_1
     */
    public function setGrade1($grade_1)
    {
        $this->grade_1 = $grade_1;
    }

    /**
     * @return int
     */
    public function getGrade2()
    {
        return $this->grade_2;
    }

    /**
     * @param int $grade_2
     */
    public function setGrade2($grade_2)
    {
        $this->grade_2 = $grade_2;
    }

    /**
     * @return int
     */
    public function getGrade3()
    {
        return $this->grade_3;
    }

    /**
     * @param int $grade_3
     */
    public function setGrade3($grade_3)
    {
        $this->grade_3 = $grade_3;
    }

    /**
     * @return int
     */
    public function getGrade4()
    {
        return $this->grade_4;
    }

    /**
     * @param int $grade_4
     */
    public function setGrade4($grade_4)
    {
        $this->grade_4 = $grade_4;
    }


    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }


}