<?php

namespace Quantox\Model;

use Quantox\Core\BaseModel;

/**
 * Class SchoolBoard
 * @package Quantox\Model
 */
Class SchoolBoard extends BaseModel
{
    /**
     * @var integer
     */
    private $schoolboard_id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $format;

    /**
     * @var array
     */
    private $settings = array(
        'fields' => ['type', 'format'],
        'table_name' => 'school_board',
        'sql' => '(`school_board_id` INT AUTO_INCREMENT NOT NULL, `type` varchar(32) NOT NULL,`format` varchar(32) NOT NULL, PRIMARY KEY (`school_board_id`))',
    );

    /**
     * @return int
     */
    public function getSchoolboardId()
    {
        return $this->schoolboard_id;
    }

    /**
     * @param int $schoolboard_id
     */
    public function setSchoolboardId($schoolboard_id)
    {
        $this->schoolboard_id = $schoolboard_id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }





}