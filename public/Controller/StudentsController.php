<?php

namespace Quantox\Controller;

use Quantox\Core\BaseController;
use Quantox\Model\SchoolBoard;
use Quantox\Model\Student;
use Quantox\RenderData\RenderData;

/**
 * Class StudentsController
 * @package Quantox\Controller
 */
class StudentsController extends BaseController{
    /**
     * @param $id
     */
    public function show($id){

        $student = new Student();
        /** @var Student $s */
        $s = $student->read($id);
        $renderData = new RenderData($s->getSchoolBoard());
        $data = $renderData->render($s);

        $schoolBoard = new SchoolBoard();
        $sb = $schoolBoard->readSchoolBoard($s->getSchoolBoard());

        $this->render($sb->getFormat(), $data);
    }
}