<?php
require "../vendor/autoload.php";

$schoolBoard = new \Quantox\Model\SchoolBoard();
$student = new \Quantox\Model\Student();

$DBmanager = new \Quantox\Core\DBAL\DBManager();
$DBmanager->createTable($schoolBoard);
$DBmanager->createTable($student);

$schoolBoardSCM = new \Quantox\Model\SchoolBoard();
$schoolBoardSCM->setType('SCM');
$schoolBoardSCM->setFormat('Json');
$schoolBoardSCM->save();

$schoolBoardSCMB = new \Quantox\Model\SchoolBoard();
$schoolBoardSCMB->setType('SCMB');
$schoolBoardSCMB->setFormat('Xml');
$schoolBoardSCMB->save();


$studentOne = new \Quantox\Model\Student();
$studentOne->setName('John');
$studentOne->setGrade1(3);
$studentOne->setGrade2(4);
$studentOne->setGrade3(6);
$studentOne->setGrade4(7);
$studentOne->setSchoolBoardId(1);
$studentOne->save();

$studentTwo = new \Quantox\Model\Student();
$studentTwo->setName('Bob');
$studentTwo->setGrade1(8);
$studentTwo->setGrade2(9);
$studentTwo->setGrade3(10);
$studentTwo->setGrade4(5);
$studentTwo->setSchoolBoardId(1);
$studentTwo->save();

$studentThree = new \Quantox\Model\Student();
$studentThree->setName('Tom');
$studentThree->setGrade1(8);
$studentThree->setGrade2(9);
$studentThree->setGrade3(10);
$studentThree->setGrade4(5);
$studentThree->setSchoolBoardId(2);
$studentThree->save();



