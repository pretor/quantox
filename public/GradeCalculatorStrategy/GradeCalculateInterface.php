<?php
namespace Quantox\GradeCalculatorStrategy;
/**
 * Interface GradeCalculateInterface
 * @package Quantox\GradeCalculatorStrategy
 */
interface GradeCalculateInterface
{
    function calculate($grades);
}