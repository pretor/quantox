<?php
namespace Quantox\GradeCalculatorStrategy;

/**
 * Class ScmbCalculateStrategy
 * @package Quantox\GradeCalculatorStrategy
 */
class ScmbCalculateStrategy implements GradeCalculateInterface
{
    public function calculate($grades)
    {
        if(sizeof($grades) > 2){
            $keyOfMinGrade = array_keys($grades,min($grades));
            unset($grades[$keyOfMinGrade[0]]);
        }

        if (max($grades) > 8) {
            return 'Pass';
        } else {
            return 'Fail';
        }

    }
}