<?php

namespace Quantox\GradeCalculatorStrategy;

/**
 * Class CalculateGrades
 * @package Quantox\GradeCalculatorStrategy
 */
class CalculateGrades implements GradeCalculateInterface
{
    /** @var GradeCalculateInterface */
    private $calculateStrategy;


    public function setStrategy(GradeCalculateInterface $strategy){
        $this->calculateStrategy = $strategy;
    }

    public function calculate($grades)
    {
        return $this->calculateStrategy->calculate($grades);
    }

    public function average($grades)
    {
        return array_sum($grades) / sizeof($grades);
    }


}