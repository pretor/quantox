<?php
namespace Quantox\GradeCalculatorStrategy;

/**
 * Class ScmCalculateStrategy
 * @package Quantox\GradeCalculatorStrategy
 */
class ScmCalculateStrategy implements GradeCalculateInterface
{
    public function calculate($grades)
    {
      $averageGrade = array_sum($grades) / sizeof($grades);
        if ($averageGrade >= 7){
            return 'Pass';
        } else {
            return 'Fail';
        }
    }
}