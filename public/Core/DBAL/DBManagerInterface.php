<?php

namespace Quantox\Core\DBAL;

use Quantox\Core\BaseModelInterface;

/**
 * Interface BaseModelInterface
 * @package Quantox\Core\DBAL
 */
interface DBManagerInterface{
  public function insert(BaseModelInterface $model);
  public function update();
  public function delete();
  public function select(BaseModelInterface $model);
}