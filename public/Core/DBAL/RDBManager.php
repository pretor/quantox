<?php
namespace Quantox\Core\DBAL;

use Quantox\Core\BaseModelInterface;
use Quantox\Model\SchoolBoard;
use Quantox\Model\Student;

/**
 * Class BaseModel
 * @package Quantox\Core\DBAL
 */
class RDBManager implements DBManagerInterface
{

    /**
     * @var \PDO
     */
    private $conn;

    function __construct()
    {
        try {
            $this->conn = new \PDO('mysql:host=localhost;dbname=homestead', 'homestead', 'secret');
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }


    public function update()
    {

    }

    public function delete()
    {

    }

    public function select(BaseModelInterface $model)
    {

    }

    /**
     * Creates table for given model
     * @param BaseModelInterface $model
     */
    public function createTable(BaseModelInterface $model)
    {
        try {
            $stmt = $this->conn->prepare("CREATE TABLE IF NOT EXISTS " . $model->getSettings()['table_name'] . $model->getSettings()['sql'] . " CHARACTER SET utf8 COLLATE utf8_general_ci");
            $stmt->execute();
            if (isset($model->getSettings()['key'])) {
                $stmt = $this->conn->prepare($model->getSettings()['key']);
                $stmt->execute();
            }
        } catch (\PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    /**
     * @param BaseModelInterface $model
     * @return mixed
     */
    public function selectStudentById(BaseModelInterface $model, $id)
    {
        try {
            $sql = "SELECT * from students where students.student_id={$id}";

            $stmt = $this->conn->prepare($sql);
            $stmt->execute();

            $row = $stmt->fetch();

            $modelClass = get_class($model);
            /** @var Student $student */
            $student = new $modelClass;
            $student->setStudentId($row['student_id']);
            $student->setName($row['name']);
            $student->setGrade1($row['grade_1']);
            $student->setGrade2($row['grade_2']);
            $student->setGrade3($row['grade_3']);
            $student->setGrade4($row['grade_4']);
            $student->setSchoolBoardId($row['school_board_id']);
            return $student;

        } catch (\PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function selectSchoolBoardById(BaseModelInterface $model, $id){
        try {
            $sql = "SELECT * from school_board where school_board.school_board_id={$id}";

            $stmt = $this->conn->prepare($sql);
            $stmt->execute();

            $row = $stmt->fetch();

            $modelClass = get_class($model);
            /** @var SchoolBoard $schoolBoard */
            $schoolBoard = new $modelClass;
            $schoolBoard->setSchoolboardId($row['school_board_id']);
            $schoolBoard->setType($row['type']);
            $schoolBoard->setFormat($row['format']);

            return $schoolBoard;

        } catch (\PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    /**
     * @param BaseModelInterface $model
     * @return boolean
     */
    public function insert(BaseModelInterface $model)
    {
        try {

            $sql = $this->_buildInsertSqlQuery($model);

            $stmt = $this->conn->prepare($sql);
            $res = $stmt->execute();

            if ($res) {
                return true;
            }

        } catch (\PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    /**
     * @param BaseModelInterface $model
     * @return string
     */
    private function _buildInsertSqlQuery(BaseModelInterface $model)
    {
        $fields = $model->getSettings()['fields'];
        $tableName = $model->getSettings()['table_name'];
        $fieldsString = $this->_arrayToString($fields);
        $values = $this->_createInsertValues($model);
        return $sql = "INSERT INTO $tableName ($fieldsString) VALUES ($values)";
    }

    /**
     * @param $fields
     * @return string
     */
    private function _arrayToString($fields)
    {
        return implode(",", $fields);
    }

    /**
     * @param $model
     * @return string
     */
    private function _createInsertValues(BaseModelInterface $model)
    {
        $values = '';

        foreach ($model->getSettings()['fields'] as $field) {
            if (strpos($field, '_') !== false) {
                $pieces = explode('_', $field);
                $fieldN = ucfirst($pieces[0]) . ucfirst($pieces[1]);
            } else {
                $fieldN = ucfirst($field);
            }
            $modelPropertyMethod = "get" . $fieldN;
            $value = $model->$modelPropertyMethod();
            $values .= "'$value',";
        }

        $values = substr($values, 0, -1);

        return $values;
    }
}