<?php
namespace Quantox\Core\DBAL;
use Quantox\Core\BaseModelInterface;

/**
 * Class BaseModel
 * @package Quantox\Core\DBAL
 */
class DBManager implements DBManagerInterface
{

  /**
   * @var \PDO
   */
  private $driver;

  function __construct()
  {
    $this->driver = new RDBManager();
  }

  /**
   * @param BaseModelInterface $model
   * @return array
   */
  public function insert(BaseModelInterface $model)
  {
    return $this->driver->insert($model);
  }

  public function update()  {
  }

  public function delete()  {

  }
  public function select(BaseModelInterface $model){

  }

  /**
   * Creates table for given model
   * @param BaseModelInterface $model
   */
  public function createTable(BaseModelInterface $model){
     $this->driver->createTable($model);
  }

    public function getStudent(BaseModelInterface $model, $id){
        return $this->driver->selectStudentById($model, $id);
    }

    public function getSchoolBoard(BaseModelInterface $model, $id){
        return $this->driver->selectSchoolBoardById($model, $id);
    }



}