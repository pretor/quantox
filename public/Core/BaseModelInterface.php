<?php

namespace Quantox\Core;
/**
 * Interface BaseModelInterface
 * @package Quantox\Core
 */
interface BaseModelInterface{
  function getSettings();
}