<?php

namespace Quantox\Core;

/**
 * Class Router
 * @package Quantox\Core
 */
class Router
{
    /**
     * @var array string
     */
    private $uri;
    /**
     * @var mixed
     */
    private $params;

    function __construct($params)
    {
        $uri_parts = explode('/', $params["REQUEST_URI"]);
        $this->uri = $uri_parts;
    }

    /**
     * Creates controller and calls method with params
     */
    public function execute()
    {
        $map = array(
            'students' => '\StudentsController',
        );

        if ($this->uri[1] == '') {
            $this->uri[1] = 'students';
        }

        $name = 'Quantox\Controller' . $map[$this->uri[1]];
        $controller = new $name;
        $methodName = 'show';
        $controller->$methodName($this->uri[2]);
    }
}
