<?php

namespace Quantox\Core;

use Quantox\Core\DBAL\DBManager;
use Quantox\Core\DBAL\DBManagerInterface;

/**
 * Class BaseModel
 * @package Quantox\Core
 */
abstract class BaseModel implements BaseModelInterface{
  /**
   * @var DBManagerInterface
   */
  private $DBmanager;

  public function __construct() {
    $this->DBmanager = new DBManager();
  }

  public function save(){
      $this->DBmanager->insert($this);
  }

  public function update(){
  }

  public function delete(){
  }

  public function read($params){
      return $this->DBmanager->getStudent($this, $params);
  }

    public function readSchoolBoard($params){
        return $this->DBmanager->getSchoolBoard($this, $params);
    }

}