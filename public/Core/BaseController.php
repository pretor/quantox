<?php

namespace Quantox\Core;

/**
 * Class BaseController
 * @package Quantox\Core
 */
abstract class BaseController
{

    /**
     * @param $data
     */
    public function renderJson($data)
    {
        header('Content-Type: application/json');
        echo $data;
    }

    /**
     * @param $data
     */
    public function renderXml($data)
    {
        header("Content-type: application/xhtml+xml");
        echo $data;
    }

    /**
     * @param $format
     * @param $data
     */
    public function render($format, $data){
        $fnName = 'render' . $format;
        $this->$fnName($data);
    }
}