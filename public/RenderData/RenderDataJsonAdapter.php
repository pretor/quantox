<?php
/**
 * Created by PhpStorm.
 * User: pretor
 * Date: 22.4.19.
 * Time: 13.31
 */

namespace Quantox\RenderData;
use Quantox\Core\BaseModelInterface;
use Quantox\Model\Student;

/**
 * Class RenderDataJsonAdapter
 * @package Quantox\RenderData
 */
class RenderDataJsonAdapter implements RenderDataInterface
{
    /**
     * @param $data
     * @return string
     */
    public function render($data)
    {
        return json_encode($data);
    }
}