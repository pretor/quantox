<?php

namespace Quantox\RenderData;
use Quantox\GradeCalculatorStrategy\CalculateGrades;
use Quantox\GradeCalculatorStrategy\ScmbCalculateStrategy;
use Quantox\GradeCalculatorStrategy\ScmCalculateStrategy;

/**
 * Class RenderData
 * @package Quantox\RenderData
 */
class RenderData implements RenderDataInterface
{
   private $renderAdapter;

    /**
     * RenderData constructor.
     * @param $param
     */
    function __construct($param) {
        if ($param == '1') {
            $this->renderAdapter = new RenderDataJsonAdapter();
        } else {
            $this->renderAdapter = new RenderDataXmlAdapter();
        }
    }

    /**
     * @param $data
     * @return string
     */
    public function render($data)
    {
       return $this->renderAdapter->render($this->_prepareData($data));
    }

    /**
     * @param $data
     * @return array
     */
    private function _prepareData($data){

        $calculateGrades = new CalculateGrades();
        if ($data->getSchoolBoard() == '1') {
            $strategy = new ScmCalculateStrategy();
        } else {
            $strategy = new ScmbCalculateStrategy();
        }
        $calculateGrades->setStrategy($strategy);
        $result = $calculateGrades->calculate([$data->getGrade1(),$data->getGrade2(),$data->getGrade3(),$data->getGrade4()]);
        $average = $calculateGrades->average([$data->getGrade1(),$data->getGrade2(),$data->getGrade3(),$data->getGrade4()]);

        $array = [
            'student_id' => $data->getStudentId(),
            'name' => $data->getName(),
            'grade_1' => $data->getGrade1(),
            'grade_2' => $data->getGrade2(),
            'grade_3' => $data->getGrade3(),
            'grade_4' => $data->getGrade4(),
            'schoolBoard' => $data->getSchoolBoard() === '1' ? 'SCM' : 'SCMB',
            'result' => $result,
            'average' => $average,
        ];
        return $array;
    }
}