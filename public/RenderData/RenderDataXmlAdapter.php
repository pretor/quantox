<?php

namespace Quantox\RenderData;

use RefactorStudio\PhpArrayToXml\PhpArrayToXml;


/**
 * Class RenderDataXmlAdapter
 * @package Quantox\RenderData
 */
class RenderDataXmlAdapter implements RenderDataInterface
{
    /**
     * @param $data
     * @return string
     */
    public function render($data)
    {
        $converter = new PhpArrayToXml();
        return $converter->prettify()->toXmlString($data);
    }
}