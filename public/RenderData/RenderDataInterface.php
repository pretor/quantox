<?php

namespace Quantox\RenderData;

/**
 * Interface RenderDataInterface
 * @package Quantox\RenderData
 */
interface RenderDataInterface
{
    function render($data);
}